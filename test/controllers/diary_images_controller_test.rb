require 'test_helper'

class DiaryImagesControllerTest < ActionController::TestCase
  setup do
    @diary_image = diary_images(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:diary_images)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create diary_image" do
    assert_difference('DiaryImage.count') do
      post :create, diary_image: { destination: @diary_image.destination, diary_id: @diary_image.diary_id, image: @diary_image.image, other: @diary_image.other, team_id: @diary_image.team_id, title: @diary_image.title, user_id: @diary_image.user_id }
    end

    assert_redirected_to diary_image_path(assigns(:diary_image))
  end

  test "should show diary_image" do
    get :show, id: @diary_image
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @diary_image
    assert_response :success
  end

  test "should update diary_image" do
    patch :update, id: @diary_image, diary_image: { destination: @diary_image.destination, diary_id: @diary_image.diary_id, image: @diary_image.image, other: @diary_image.other, team_id: @diary_image.team_id, title: @diary_image.title, user_id: @diary_image.user_id }
    assert_redirected_to diary_image_path(assigns(:diary_image))
  end

  test "should destroy diary_image" do
    assert_difference('DiaryImage.count', -1) do
      delete :destroy, id: @diary_image
    end

    assert_redirected_to diary_images_path
  end
end
