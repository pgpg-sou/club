require 'test_helper'

class TechLogsControllerTest < ActionController::TestCase
  setup do
    @tech_log = tech_logs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:tech_logs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create tech_log" do
    assert_difference('TechLog.count') do
      post :create, tech_log: { destination: @tech_log.destination, other: @tech_log.other, tag: @tech_log.tag, title: @tech_log.title, video: @tech_log.video }
    end

    assert_redirected_to tech_log_path(assigns(:tech_log))
  end

  test "should show tech_log" do
    get :show, id: @tech_log
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @tech_log
    assert_response :success
  end

  test "should update tech_log" do
    patch :update, id: @tech_log, tech_log: { destination: @tech_log.destination, other: @tech_log.other, tag: @tech_log.tag, title: @tech_log.title, video: @tech_log.video }
    assert_redirected_to tech_log_path(assigns(:tech_log))
  end

  test "should destroy tech_log" do
    assert_difference('TechLog.count', -1) do
      delete :destroy, id: @tech_log
    end

    assert_redirected_to tech_logs_path
  end
end
