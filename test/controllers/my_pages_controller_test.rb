require 'test_helper'

class MyPagesControllerTest < ActionController::TestCase
  setup do
    @my_page = my_pages(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:my_pages)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create my_page" do
    assert_difference('MyPage.count') do
      post :create, my_page: {  }
    end

    assert_redirected_to my_page_path(assigns(:my_page))
  end

  test "should show my_page" do
    get :show, id: @my_page
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @my_page
    assert_response :success
  end

  test "should update my_page" do
    patch :update, id: @my_page, my_page: {  }
    assert_redirected_to my_page_path(assigns(:my_page))
  end

  test "should destroy my_page" do
    assert_difference('MyPage.count', -1) do
      delete :destroy, id: @my_page
    end

    assert_redirected_to my_pages_path
  end
end
