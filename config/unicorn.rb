worker_processes 2
 
listen "/var/run/unicorn/unicorn_club.sock"
pid "/var/run/unicorn/unicorn_club.pid" 

stderr_path  "/var/run/unicorn/unicorn.stderr.log"
stdout_path "/var/run/unicorn/unicorn.stdout.log"

preload_app true
