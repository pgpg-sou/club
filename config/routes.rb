Run::Application.routes.draw do

  devise_for :general_users , :controllers => { :registrations => "general_users/registrations" , :sessions => "general_users/sessions" }
  resources :comments

  resources :records

  resources :team_albums

  resources :diary_images

  resources :teams

  resources :diaries

  resources :users
  resources :my_teams

  get "/auth/:provider/callback" => "sessions#callback"
  get '/index' => 'my_pages#index'
  get "teams/pass_check"
  post "/login" => "sessions#check"
  get '/sign_up' => "devise/registrations#new"
  post "teams/pass_check"
  post "teams/show"
  post 'my_pages/like'
  post 'my_pages/confirm'
  post 'my_teams/confirm'
  post 'my_teams/like'
  post 'diaries/data_save'
  post 'comments/save_comment' 
  get '/my_teams' => 'my_teams#index'
  get 'my_pages/like'
  get 'lps/index' => 'lps#index'
  get 'members/index'


  root :to => "my_pages#index"

  resources :tech_logs
  resources :members

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
