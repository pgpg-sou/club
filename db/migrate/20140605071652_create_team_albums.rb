class CreateTeamAlbums < ActiveRecord::Migration
  def change
    create_table :team_albums do |t|
      t.string :image
      t.string :title
      t.string :destination
      t.string :other
      t.integer :team_id
      t.integer :user_id

      t.timestamps
    end
  end
end
