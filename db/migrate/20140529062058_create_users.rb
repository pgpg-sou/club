class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :uid
      t.string :name
      t.string :birthday
      t.string :image
      t.string :destination
      t.string :sex
      t.string :password
      t.string :sporting_event
      t.integer :team_id

      t.timestamps
    end
  end
end
