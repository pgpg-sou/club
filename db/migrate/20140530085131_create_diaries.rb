class CreateDiaries < ActiveRecord::Migration
  def change
    create_table :diaries do |t|
      t.string :menu
      t.string :date
      t.string :subtitle
      t.string :other
      t.integer :user_id
      t.integer :team_id
      t.string :image
      t.integer :likes

      t.timestamps
    end
  end
end
