class CreateRecords < ActiveRecord::Migration
  def change
    create_table :records do |t|
      t.string :title
      t.string :time
      t.integer :user_id
      t.string :diary_id
      t.string :lap
      t.string :rlp

      t.timestamps
    end
  end
end
