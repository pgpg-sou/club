class CreateTechLogs < ActiveRecord::Migration
  def change
    create_table :tech_logs do |t|
      t.string :title
      t.string :destination
      t.string :tag
      t.string :video
      t.string :other
      t.integer :team_id
      t.integer :user_id

      t.timestamps
    end
  end
end
