class CreateDiaryImages < ActiveRecord::Migration
  def change
    create_table :diary_images do |t|
      t.string :image
      t.integer :team_id
      t.integer :diary_id
      t.integer :user_id
      t.string :title
      t.string :destination
      t.string :other

      t.timestamps
    end
  end
end
