class CreateTeams < ActiveRecord::Migration
  def change
    create_table :teams do |t|
      t.string :name
      t.string :destination
      t.string :image
      t.string :other
      t.string :password_digest

      t.timestamps
    end
  end
end
