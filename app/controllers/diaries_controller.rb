
class DiariesController < ApplicationController
  before_action :set_diary, only: [:show, :edit, :update, :destroy]
  helper_method :data_save, :record_params

  include ApplicationHelper

  # GET /diaries
  # GET /diaries.json
  def index
    @diaries = Diary.all

  end

  def data_save
    logger.debug(params[:photoname])

    @diary_image = DiaryImage.new
    @diary_image.image = params[:photoname]

    @diary_image.save
  end

  # GET /diaries/1
  # GET /diaries/1.json
  def show
  end

  # GET /diaries/new
  def new
    @easy = ['たのしかったけど、つかれた。', 'めちゃめちゃ辛かったけど、楽しかったです。',
          'ぶっちゃけそんな辛くなかった。', '楽しくなかった。。ただつらいだけ。',
          '新しい境地にたどり着けた気がする']

    day = Time.now
    @all_diary = Diary.all
    @diary = Diary.new
    @diary_image = DiaryImage.new
    user = user();

    logger.debug 'sample'
    logger.debug @all_diary.size
    logger.debug user.team_id

    @diary.date = day.strftime('%Y-%m-%d');
    @diary_image.team_id = user.team_id
    @diary_image.user_id = user.id
    @diary_image.diary_id = @all_diary.size + 1

    if(session[:general_user_id] != nil)
	@diary.likes = user.id
    else
	@diary.user_id = user.id
    end
    @diary.team_id = user.team_id
  end


  # GET /diaries/1/edit
  def edit
    @easy = ['たのしかったけど、つかれた。', 'めちゃめちゃ辛かったけど、楽しかったです。',
          'ぶっちゃけそんな辛くなかった。', '楽しくなかった。。ただつらいだけ。',
          '新しい境地にたどり着けた気がする']

    logger.debug @diary
    @record = Record.find_by :diary_id => @diary.id

    logger.debug @diary.other
  end

  # POST /diaries
  # POST /diaries.json
  def create
    diary_length = Diary.all
    @diary = Diary.new(diary_params)


    @record = Record.new

    @record.title = params[:diary][:subtitle]
    @record.time = params[:time]
    @record.user_id = params[:diary][:user_id]
    @record.diary_id = diary_length.size + 1
    @record.save
    logger.debug params[:diary][:menu]
    logger.debug 'params'
    logger.debug params[:diary][:id]
    if @diary.save
      redirect_to index_path
    else
      redirect_to new_diary_path, :notice => "入力エラーがあります"
    end
  end

  # PATCH/PUT /diaries/1
  # PATCH/PUT /diaries/1.json
  def update
    if @diary.update(diary_params)
      redirect_to index_path
    end
  end

  # DELETE /diaries/1
  # DELETE /diaries/1.json
  def destroy
    @diary.destroy
    respond_to do |format|
      format.html { redirect_to diaries_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_diary
      @diary = Diary.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def diary_params
      params.require(:diary).permit(:menu, :date, :subtitle, :other, :user_id, :team_id, :image, :likes)
    end
end
