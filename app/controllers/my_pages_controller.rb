class MyPagesController < ApplicationController
  before_action :set_my_page, only: [:show, :edit, :update, :destroy]
  helper_method :like, :current_diary, :getsubTitle, :getData, :getCreateTime
  include ApplicationHelper

  before_action :authenticate_general_user!, :only => [:new, :create, :update]
  @current_diary
  @title
  # GET /my_pages
  # GET /my_pages.json
  def index

    if !login?
      logger.debug "login failed"
      redirect_to new_general_user_session_path

    else
      @user = user()

      if session[:general_user_id] != nil
	@diary = Diary.find(:all, :conditions => { :likes => @user.id})
      else 
      	@diary = Diary.find(:all, :conditions => { :user_id => @user.id })
      end
      @diary = @diary.reverse

      logger.debug session[:general_user_id]
      logger.debug session[:user_id]

      @diary_image = DiaryImage.new
      @comment = Comment.new
    end
       
  end

  def like
    like_do(params[:id])
  end

  def confirm
    logger.debug 'success'
    logger.debug current_diary(params[:id])
    user = user()
    @title = params[:id]
    @comment_lists = Comment.find(:all, :conditions => { :diary_id => @current_diary.id})
    
    @comment = Comment.new
    @comment.user_id = user.id  
    @comment.diary_id = @current_diary.id

    render
  end

  # GET /my_pages/1:w
  # GET /my_pages/1.json
  def show
  end

  # GET /my_pages/new
  def new
    @my_page = MyPage.new
  end

  # GET /my_pages/1/edit
  def edit
  end

  # POST /my_pages
  # POST /my_pages.json
  def create
    @my_page = MyPage.new(my_page_params)

    logger.debug(my_page_params)

    respond_to do |format|
      if @my_page.save
        format.html { redirect_to @my_page, notice: 'My page was successfully created.' }
        format.json { render action: 'show', status: :created, location: @my_page }
      else
        format.html { render action: 'new' }
        format.json { render json: @my_page.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /my_pages/1
  # PATCH/PUT /my_pages/1.json
  def update
    respond_to do |format|
      if @my_page.update(my_page_params)
        format.html { redirect_to @my_page, notice: 'My page was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @my_page.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /my_pages/1
  # DELETE /my_pages/1.json
  def destroy
    @my_page.destroy
    respond_to do |format|
      format.html { redirect_to my_pages_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_my_page
      @my_page = MyPage.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def my_page_params
      params[:my_page]
    end

    def diary_image_params

    end

    def like_do(diary_id = 1)
      logger.debug diary_id
      user = User.find_by :id => session[:user_id]
      diary = Diary.find_by :id => diary_id
      logger.debug 'called like helper method by views'

      begin 
        diary.add_evaluation(:likes, 1, user)
        redirect_to index_path
      rescue
        num = diary.reputation_for(:likes).to_i
        diary.add_or_update_evaluation(:likes, num - 1, user)
        redirect_to index_path
      end
    end

    def current_diary(diary_id) 
      @diary = Diary.find_by :id => diary_id
      @current_diary = @diary
      return diary_id
    end   

    def getsubTitle() 
      return @current_diary.subtitle
    end

    def getData()
      @records = Record.find(:all, :conditions => {:title => @current_diary.subtitle})

      data = []
      @records.each do |r|
        timeAry = r.time.split(".")
        logger.debug timeAry[1]
        logger.debug timeAry[1].to_f / 60
        result = timeAry[0].to_f + timeAry[1].to_f/60
        data.push result
      end


      return data
    end

    def getCreateTime() 
      @records = Record.find(:all, :conditions => {:title => @current_diary.subtitle})
      data = []
      
      @records.each do |r|
        data.push r.created_at.strftime('%m-%d %H:%M')
      end

      logger.debug(data)
      return data
    end
end
