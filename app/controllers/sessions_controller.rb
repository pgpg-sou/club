class SessionsController < ApplicationController

  def callback
    auth = request.env["omniauth.auth"]
    @user_auth = auth

    logger.debug(auth)
    user = User.find_by(:uid => auth["uid"])

    logger.debug(auth["info"]["uid"])
    logger.debug(auth["uid"])
    logger.debug("this is sample")

    if user
	logger.debug("found")
        session[:user_id] = user.id
        redirect_to :controller => 'my_pages', :action => 'index'
        
    else
	logger.debug("not found")
	User.create_user(auth)
	redirect_to :controller => 'users', :action => 'new'
    end 
  end

  def check
    logger.debug("call check function")
    logger.debug params[:password]

    user = GeneralUser.find_by(:email => params[:email])

    logger.debug user.encrypted_password
    if user && user.encrypted_password == params[:password]
        logger.debug 'success'
    else
        logger.debug 'failed'
    end


  end

  def create
    team = Team.find_by_name params[:name]
    
    if team && team.authenticate(params[:pass])
        session[:team_id] = team.id
        redirect_to root_path
    else
        # flash.now.alert = "Invalid"
        # render "new"
    end
  end

end
