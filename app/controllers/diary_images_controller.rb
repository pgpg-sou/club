class DiaryImagesController < ApplicationController
  before_action :set_diary_image, only: [:show, :edit, :update, :destroy]

  # GET /diary_images
  # GET /diary_images.json
  def index
    @diary_images = DiaryImage.all
  end

  # GET /diary_images/1
  # GET /diary_images/1.json
  def show
  end

  new
  def new
    @diary_image = DiaryImage.new
  end

  # GET /diary_images/1/edit
  def edit
  end

  # POST /diary_images
  # POST /diary_images.json
  def create
    @diary_image = DiaryImage.new(diary_image_params)
    
  end

  # PATCH/PUT /diary_images/1
  # PATCH/PUT /diary_images/1.json
  def update
    respond_to do |format|
      if @diary_image.update(diary_image_params)
        format.html { redirect_to @diary_image, notice: 'Diary image was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @diary_image.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /diary_images/1
  # DELETE /diary_images/1.json
  def destroy
    @diary_image.destroy
    respond_to do |format|
      format.html { redirect_to diary_images_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_diary_image
      @diary_image = DiaryImage.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def diary_image_params
      params.require(:diary_image).permit(:image, :team_id, :diary_id, :user_id, :title, :destination, :other)
    end
end
