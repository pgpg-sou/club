class TeamAlbumsController < ApplicationController
  before_action :set_team_album, only: [:show, :edit, :update, :destroy]

  # GET /team_albums
  # GET /team_albums.json
  def index
    @team_albums = TeamAlbum.all
  end

  # GET /team_albums/1
  # GET /team_albums/1.json
  def show
  end

  # GET /team_albums/new
  def new
    @team_album = TeamAlbum.new
  end

  # GET /team_albums/1/edit
  def edit
  end

  # POST /team_albums
  # POST /team_albums.json
  def create
    @team_album = TeamAlbum.new(team_album_params)
    user = User.find_by(:id => session[:user_id])
    @team_album.user_id = user.id
    @team_album.team_id = user.team_id

    if @team_album.save
      redirect_to my_teams_path
    else
      redirect_to new_team_album_path, :notice => "入力エラーがあります"
    end
  end

  # PATCH/PUT /team_albums/1
  # PATCH/PUT /team_albums/1.json
  def update
    respond_to do |format|
      if @team_album.update(team_album_params)
        format.html { redirect_to @team_album, notice: 'Team album was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @team_album.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /team_albums/1
  # DELETE /team_albums/1.json
  def destroy
    @team_album.destroy
    respond_to do |format|
      format.html { redirect_to team_albums_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_team_album
      @team_album = TeamAlbum.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def team_album_params
      params.require(:team_album).permit(:image, :title, :destination, :other, :team_id, :user_id)
    end
end
