class MyTeamsController < ApplicationController
	helper_method :current_diary, :getsubTitle, :getData, :getCreateTime
	include ApplicationHelper

	def index
		@user = user();

	    if @user.team_id == nil
	    	redirect_to ({:controller => "teams", :action =>"index"}), :notice => "チームに入ってください"
		else 
			@diary = Diary.find(:all, :conditions => {:team_id => @user.team_id})
 		    @diary = @diary.reverse
			@team = Team.find_by :id => @user.team_id
			@teams_photo = TeamAlbum.find(:all, :conditions => {:team_id => @user.team_id})
			@comment = Comment.new
		end
	end

	def like
    	like_do(params[:id])

    	render
	end


	def confirm
	    logger.debug 'success'
	    logger.debug current_diary(params[:id])
	    @title = params[:id]
	    @comment_lists = Comment.find(:all, :conditions => { :diary_id => @current_diary.id})
	    
	    @comment = Comment.new
	    @comment.user_id = session[:user_id]
	    @comment.diary_id = @current_diary.id

	    render
 	end


 	def like(diary_id = 1)
      logger.debug diary_id
      user = user();
      @diary = Diary.find_by :id => diary_id
      logger.debug 'called like helper method by views'
      num = @diary.reputation_for(:likes).to_i

      begin 
        @diary.add_evaluation(:likes, num + 1, user)
      rescue
        @diary.add_or_update_evaluation(:likes, num-1, user)
      end
    end
    private
 	    def current_diary(diary_id) 
    	  @diary = Diary.find_by :id => diary_id
      	  @current_diary = @diary
	      return diary_id
	    end   

	    def getsubTitle() 
	      return @current_diary.subtitle
	    end

	    def getData()
	      @records = Record.find(:all, :conditions => {:title => @current_diary.subtitle})

	      data = []
	      @records.each do |r|
	        timeAry = r.time.split(".")
	        logger.debug timeAry[1]
	        logger.debug timeAry[1].to_f / 60
	        result = timeAry[0].to_f + timeAry[1].to_f/60
	        data.push result
	      end


	      return data
	    end

	    def like_do(diary_id = 1)
	      logger.debug diary_id
	      user = user();
	      user = User.find_by :id => session[:user_id]
	      diary = Diary.find_by :id => diary_id
	      logger.debug 'called like helper method by views'

	      begin 
	        diary.add_evaluation(:likes, 1, user)
	        redirect_to index_path
	      rescue
	        diary.add_or_update_evaluation(:likes, -1, user)
	        redirect_to index_path
	      end
	    end


	    def getCreateTime() 
	      @records = Record.find(:all, :conditions => {:title => @current_diary.subtitle})
	      data = []
	      
	      @record.each do |r|
	        data.push r.created_at
	      end

	      return data
	    end

end
