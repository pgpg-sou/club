class GeneralUsers::SessionsController < Devise::SessionsController

	def new
		super
	end

	def create

		user = GeneralUser.find_by(:email => params[:general_user][:email])
		logger.debug user.destination
		session[:general_user_id] = user.id
		session[:user_id] = nil

		if user && general_user_signed_in?
			logger.debug "success"
			redirect_to index_path

		else
			logger.debug "failed"
		end

	end
end
