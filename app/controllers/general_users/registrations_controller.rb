class GeneralUsers::RegistrationsController < Devise::RegistrationsController
	before_filter :configure_permitted_parameters


  def edit
    super
  end

  def update
    super
  end

  def new
  	super

    logger.debug "RegistrationsController#new"
    logger.debug session[:user_id]
  end
 
  def create
    super

    logger.debug "RegistrationsController#create"
    logger.debug params[:general_user][:email]
    logger.debug params[:general_user][:name]

    user = GeneralUser.find_by(:email => params[:general_user][:email])

    logger.debug user.id
    session[:general_user_id] = user.id
    session[:user_id] = nil
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:account_update) { |u| 
      u.permit(:password, :password_confirmation,:current_password, :name, :destination,:sex , :image )
    }

    devise_parameter_sanitizer.for(:sign_up).push(:name, :destination, :sex, :sporting_event, :team_id, :image)
  end

end