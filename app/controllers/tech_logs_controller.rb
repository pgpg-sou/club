class TechLogsController < ApplicationController
  before_action :set_tech_log, only: [:show, :edit, :update, :destroy]

  include ApplicationHelper

  def tag_cloud
    # order('count DESC')でカウントの多い順にタグを並べています
    @tags = TechLog.tag_counts_on(:tags).order('count DESC')
  end

  # GET /tech_logs
  # GET /tech_logs.json
  def index
    user = user()
    @tech_logs = TechLog.find(:all, :conditions => {:team_id => user.team_id})
  end

  # GET /tech_logs/1
  # GET /tech_logs/1.json
  def show
  end

  # GET /tech_logs/new
  def new
    @tech_log = TechLog.new
    @user = user()

    @tech_log.user_id = @user.id
    @tech_log.team_id = @user.team_id
  end

  # GET /tech_logs/1/edit
  def edit
  end

  # POST /tech_logs
  # POST /tech_logs.json
  def create
    @tech_log = TechLog.new(tech_log_params)


    if @tech_log.save
      redirect_to tech_logs_path
    else 
      redirect_to new_tech_log_path, :notice => "入力エラーがあります"
    end
  end

  # PATCH/PUT /tech_logs/1
  # PATCH/PUT /tech_logs/1.json
  def update
    respond_to do |format|
      if @tech_log.update(tech_log_params) && @tech_logtech.video.recreate_versions!
        format.html { redirect_to @tech_log, notice: 'Tech log was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @tech_log.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tech_logs/1
  # DELETE /tech_logs/1.json
  def destroy
    @tech_log.destroy
    respond_to do |format|
      format.html { redirect_to tech_logs_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tech_log
      @tech_log = TechLog.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tech_log_params
      params.require(:tech_log).permit(:title, :destination, :tag, :video, :other, :user_id, :team_id)
    end
end
