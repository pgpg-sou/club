class TeamsController < ApplicationController
  before_action :set_team, only: [:show, :edit, :update, :destroy]
  include ApplicationHelper

  def pass_check
    logger.debug('get ajax data')
    logger.debug(params[:pass])
    # logger.debug(params[:id])

    team = Team.find_by( :id => params[:current])
    user = user();

    if team && team.authenticate(params[:pass])
      user.team_id = team.id
      @diary = Diary.find(:all, :conditions => { :user_id => user.id})
      @tech_log = TechLog.find(:all, :conditions => { :user_id => user.id})

      @diary.each do |diary|
        diary.team_id = team.id
        diary.save
      end

      @tech_log.each do |tech|
        tech.team_id = team.id
        tech.save
      end
      user.save
      logger.debug 'success'

      respond_to do |format|
        format.js { render :file => "/teams/pass_check.js.erb" }
        format.json { render json: 'success'}
      end
    else
      logger.debug 'false'
      respond_to do |format|
        format.js { render :file => "/teams/pass_check_failed.js.erb" }
      end
    end
  end

  # GET /teams
  # GET /teams.json
  def index
    @teams = Team.all
    
  end

  # GET /teams/1
  # GET /teams/1.json
  def show
  end

  # GET /teams/new
  def new
    @team = Team.new
  end

  # GET /teams/1/edit
  def edit
  end

  # POST /teams
  # POST /teams.json
  def create
    @team = Team.new(team_params)
    logger.debug(team_params)

    if @team.save
      redirect_to teams_path
    else
      redirect_to new_team_path, :notice => "入力エラーがあります。"
    end
  end

  # PATCH/PUT /teams/1
  # PATCH/PUT /teams/1.json
  def update
    if @team.update(team_params)
      redirect_to teams_path
    end
  end

  # DELETE /teams/1
  # DELETE /teams/1.json
  def destroy
    @team.destroy
    respond_to do |format|
      format.html { redirect_to teams_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_team
      @team = Team.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def team_params
      params.require(:team).permit(:name, :destination, :image, :other, :password ,:password_confirmation)
    end
end
