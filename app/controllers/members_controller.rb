class MembersController < ApplicationController
	include ApplicationHelper

	def index
		@user = user()
		@team = Team.find_by :id => @user.team_id
		@member = User.find(:all, :conditions =>{ :team_id => @team.id})
	end

	def show
		@user = user()
		@diary = Diary.find(:all, :conditions => { :user_id => params[:id]});

		logger.debug @diary
	end
end
