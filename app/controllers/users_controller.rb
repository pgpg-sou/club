class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  # GET /users
  # GET /users.json
  def index
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
  end

  # GET /users/new
  def new
    @user = User.new
    data = User.getData()

    logger.debug(data["uid"])

    @user.uid = data["uid"]
    @user.name = data["info"]["name"]
    @user.birthday = data["extra"]["raw_info"]["birthday"]
    @user.image = data["info"]["image"]
    @user.sex = data["extra"]["raw_info"]["gender"]
    @user.password = data["provider"]

    logger.debug("raw_info")
    logger.debug(data["extra"])
  end

  # GET /users/1/edit
  def edit
    @user = User.find_by :id => session[:user_id]
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)

    if @user.save
      redirect_to :controller => 'lps', :action => 'index'
    else
      redirect_to new_user_path, :notice => "入力エラーがあります"
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    
    if @user.update(user_params)
      redirect_to :controller => 'my_pages', :action => 'index'
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:uid, :name, :birthday, :image, :destination, :sex, :password, :sporting_event)
    end
end


