json.array!(@team_albums) do |team_album|
  json.extract! team_album, :id, :image, :title, :destination, :other, :team_id, :user_id
  json.url team_album_url(team_album, format: :json)
end
