json.array!(@teams) do |team|
  json.extract! team, :id, :name, :destination, :image, :other, :password_digest
  json.url team_url(team, format: :json)
end
