json.array!(@diaries) do |diary|
  json.extract! diary, :id, :menu, :date, :subtitle, :other, :user_id, :team_id, :time
  json.url diary_url(diary, format: :json)
end
