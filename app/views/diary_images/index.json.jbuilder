json.array!(@diary_images) do |diary_image|
  json.extract! diary_image, :id, :image, :team_id, :diary_id, :user_id, :title, :destination, :other
  json.url diary_image_url(diary_image, format: :json)
end
