json.array!(@records) do |record|
  json.extract! record, :id, :title, :time, :user_id, :diary_id, :lap, :rlp
  json.url record_url(record, format: :json)
end
