json.array!(@tech_logs) do |tech_log|
  json.extract! tech_log, :id, :title, :destination, :tag, :video, :other
  json.url tech_log_url(tech_log, format: :json)
end
