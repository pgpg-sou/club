json.array!(@users) do |user|
  json.extract! user, :id, :name, :birthday, :image, :destination, :sex, :password, :sporting_event
  json.url user_url(user, format: :json)
end
