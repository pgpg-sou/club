json.array!(@my_pages) do |my_page|
  json.extract! my_page, :id
  json.url my_page_url(my_page, format: :json)
end
