class TechLog < ActiveRecord::Base
	mount_uploader :video, VideoUploader

	validates :title, presence: true
	validates :video, presence: true
end
