class TeamAlbum < ActiveRecord::Base
	mount_uploader :image, ImageUploader
	validates :image, presence: true
	validates :title, presence: true
end
