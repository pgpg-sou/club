class Team < ActiveRecord::Base
	mount_uploader :image, ImageUploader
	has_secure_password 
	validates :name, presence: true
	validates :destination , presence: true
	validates :image, presence: true
end
