class User < ActiveRecord::Base
	mount_uploader :image, ImageUploader
	validates :name, presence: true
	validates :destination, presence: true

	@@auth_data

	def self.create_user(auth)
		logger.debug(auth)
		@@auth_data = auth

		logger.debug("success")
	end

	def self.getData() 
		return @@auth_data
	end
end
