class Diary < ActiveRecord::Base
	mount_uploader :image, ImageUploader
	belongs_to :users
	has_reputation :likes, source: :user, aggregated_by: :sum

	validates :menu, presence: true
	validates :date, presence: true
	validates :subtitle, presence: true
	validates :other, presence: true


end
